package com.athmin.controller;

import java.util.Locale;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jettison.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.quicksight.AmazonQuickSight;
import com.amazonaws.services.quicksight.AmazonQuickSightClientBuilder;
import com.amazonaws.services.quicksight.model.GetDashboardEmbedUrlRequest;
import com.amazonaws.services.quicksight.model.GetDashboardEmbedUrlResult;
import com.amazonaws.services.quicksight.model.IdentityType;
import com.athmin.constants.Constants;

@RestController
public class EmbeddedUrlGenerator {

	@RequestMapping(value = "/api/dashboard/{dashboardId}/url", method = { RequestMethod.GET })
	public @ResponseBody ResponseEntity<String> addCustomer(@PathVariable("dashboardId") String dashboardId,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		JSONObject outputJson = new JSONObject();
		try {
			if (dashboardId.length() > 3) {
				ResourceBundle resourceBundle = ResourceBundle.getBundle("application", Locale.US);
				String awsAccountId = resourceBundle.getString(Constants.AWS_ACCOUNT_ID);
				AmazonQuickSight client = getClient();
				if (client == null) {
					outputJson.put(Constants.MESSAGE, "client is not available.");
					outputJson.put(Constants.EMBEDDED_URL, Constants.NA);
				} else {
					outputJson.put(Constants.MESSAGE, "client is available.");
					String validityTime = resourceBundle
							.getString(Constants.DASHBOARD_EMBEDDED_URL_TIME_VALIDITY_IN_MINUTE);
					final GetDashboardEmbedUrlResult dashboardEmbedUrlResult = client
							.getDashboardEmbedUrl(new GetDashboardEmbedUrlRequest().withDashboardId(dashboardId)
									.withAwsAccountId(awsAccountId).withIdentityType(IdentityType.IAM)
									.withResetDisabled(true).withSessionLifetimeInMinutes(Long.parseLong(validityTime))
									.withUndoRedoDisabled(false));
					if (dashboardEmbedUrlResult != null) {
						String embeddedUrl = dashboardEmbedUrlResult.getEmbedUrl();
						outputJson.put(Constants.EMBEDDED_URL, embeddedUrl);
					} else {
						outputJson.put(Constants.EMBEDDED_URL, Constants.NA);
					}
				}
			} else {
				outputJson.put(Constants.ERROR, "not a valid dashboardId");
			}
		} catch (Exception e) {
			outputJson.put(Constants.ERROR, e.getMessage());
			return new ResponseEntity<String>(outputJson.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<String>(outputJson.toString(), HttpStatus.OK);
	}

	private static AmazonQuickSight getClient() {

		final AWSCredentialsProvider credsProvider = new AWSCredentialsProvider() {
			@Override
			public AWSCredentials getCredentials() {
				// provide actual IAM access key and secret key here
				ResourceBundle resourceBundle = ResourceBundle.getBundle("application", Locale.US);
				String accessKey = resourceBundle.getString(Constants.ACCESS_KEY);
				String secretKey = resourceBundle.getString(Constants.SECRET_KEY);

				return new BasicAWSCredentials(accessKey, secretKey);
			}

			@Override
			public void refresh() {
			}
		};

		return AmazonQuickSightClientBuilder.standard().withRegion(Regions.AP_SOUTHEAST_1.getName())
				.withCredentials(credsProvider).build();
	}

}
