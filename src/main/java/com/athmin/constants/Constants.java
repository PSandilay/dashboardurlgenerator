package com.athmin.constants;

public interface Constants {
String ACCESS_KEY = "access_key";
String SECRET_KEY = "secret_key";
String AWS_ACCOUNT_ID = "aws_account_id";
String EMBEDDED_URL = "embedded_url";
String ERROR = "error";
String MESSAGE = "message";
String DASHBOARD_EMBEDDED_URL_TIME_VALIDITY_IN_MINUTE = "dashboard_embedded_url_time_validity_in_minute";
String NA="N/A";
}
