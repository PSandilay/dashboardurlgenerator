package com.athmin.dashboardUrlGenerator;

/**
 * Hello world!
 *
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Created by vvishnoi on 4/3/18.
 */
@SpringBootApplication(scanBasePackages = {"com.athmin"})
@Configuration
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }

    @Bean
    public WebMvcConfigurer corsConfigurer(){
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**").allowedOrigins("*").
                        allowedMethods( HttpMethod.PUT.name(),
                                        HttpMethod.POST.name(),
                                        HttpMethod.DELETE.name(),
                                        HttpMethod.OPTIONS.name(),
                                        HttpMethod.GET.name(),
                                        HttpMethod.PATCH.name(),
                                        HttpMethod.HEAD.name());
            }
        };
    }
}